# AF-2DEngine

#### Description
Alchemy Furnace 2D engine for javascript.
AF2DEngine是基于canvas下开发的动画引擎。
遵循场景，图层，精灵的动画设计模式
目前精灵类型支持：
线，多边形，圆形，自定义图片。
内置线，多边形，圆形的物理碰撞测试引擎。
支持click,mousedown,mousemove,mouseup事件监听。
演示demo:
基于神经网络+遗传算法实现AI塞车游戏
http://119.3.123.193:8011/AICar
地图编辑器:
http://119.3.123.193:8011/MapEditor


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
