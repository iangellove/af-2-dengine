# AF-2DEngine

#### 介绍
Alchemy Furnace 2D engine for javascript.

AF2DEngine是基于canvas下开发的动画引擎。

遵循场景，图层，精灵的动画设计模式

目前精灵类型支持：
线，多边形，圆形，自定义图片。

内置线，多边形，圆形的物理碰撞测试引擎。

支持click,mousedown,mousemove,mouseup事件监听。

演示demo:

基于神经网络+遗传算法实现AI赛车游戏

http://119.3.123.193:8011/AICar

地图编辑器:

http://119.3.123.193:8011/MapEditor